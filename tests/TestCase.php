<?php

namespace Tests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\UploadedFile;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TestCase extends BaseTestCase {
    use DatabaseTransactions;

    private $token = null;
    private $headers = [
        'token' => null,
    ];

    /* Creates the application. */
    public function createApplication() {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function test_api_can_flush_caches() {
        $this->withoutMiddleware();
        $req = $this->get( 'flush' );
        $req->assertResponseOk();
    }

    /* Helpers */
    public function log( $request ) {
        $e = $request->response->exception;
        if ( $e ) {
            dd( $request->response->exception->getMessage() );
        }
    }

    protected function jwt( User $user ) {
        // Time for 10 min
        $time = 600;
        $payload = [
            'iss' => 'laravel-testing-jwt',
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + $time,
        ];
        return JWT::encode( $payload, env( 'JWT_SECRET' ) );
    }

    public function setSession( User $user = null ) {
        if ( $this->token == null ) {
            if ( !$user ) {
                $user = $this->getRootUser();
            }
            $this->token = $this->jwt( $user );
            $this->headers[ 'token' ] = $this->token;
        }
    }

    public function getRootUser() {
        return User::where( 'type', User::ROOT )->first();
    }

    public function getRandom( $class, int $qty = 1 ) {
        if ( $qty <= 1 ) {
            return $class::inRandomOrder()->first();
        }
        return $class::inRandomOrder()->take( $qty )->get();
    }

    public function getFakeImg( string $filename = 'image.jpg' ) {
        return UploadedFile::fake()->image( $filename );
    }

    /* Request testing helpers */
    public function base_get(
        string $url,
        int $status,
        array $rules,
        bool $dump = false
    ) {
        $req = $this->get( $url, $this->headers );
        if ( $dump ) { $this->log( $req ); }
        $req->seeStatusCode( $status );
        $req->seeJsonContains( $rules );
        return $req;
    }

    public function base_post(
        string $url,
        array $data,
        int $status,
        array $rules,
        bool $dump = false
    ) {
        $req = $this->post( $url, $data, $this->headers );
        if ( $dump ) { $this->log( $req ); }
        $req->seeStatusCode( $status );
        $req->seeJsonStructure( $rules );
        return $req;
    }

    public function base_patch(
        string $url,
        array $data,
        int $status,
        array $rules,
        bool $dump = false
    ) {
        $req = $this->patch( $url, $data, $this->headers );
        if ( $dump ) { $this->log( $req ); }
        $req->seeStatusCode( $status );
        $req->seeJsonStructure( $rules );
        return $req;
    }

    public function base_destroy(
        string $url,
        int $status,
        array $rules,
        bool $dump = false
    ) {
        $req = $this->delete( $url, [], $this->headers );
        if ( $dump ) { $this->log( $req ); }
        $req->seeStatusCode( $status );
        $req->seeJsonStructure( $rules );
        return $req;
    }

}
