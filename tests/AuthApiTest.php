<?php

namespace Tests;

class AuthApiTest extends TestCase {

    private $url = 'login';

    public function test_api_success_login() {
        $root = $this->getRootUser();
        $loginData = [
            'email' => $root->email,
            'password' => env( 'CONTENT_KEY' ),
        ];
        $this->base_post(
            $this->url,
            $loginData,
            200, [
                'token',
                'type',
            ]
        );
    }

}

