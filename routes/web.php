<?php

$router->post('login', 'AuthController@login');

$router->group([
    'middleware' => 'webAuth'
], function() use ($router) {
    $router->post('/mail',  'MailController@send');
    $router->get('/flush', 'Controller@flushCache');
});

$router->group([
    'prefix' => 'users',
    'middleware' => [
        'auth',
        'adminAuth',
    ],
], function() use ($router) {
    $router->get('/', 'UserController@index');
    $router->get('/{id}', 'UserController@show');
    $router->patch('/{id}', 'UserController@update');
    $router->delete('/{id}', 'UserController@destroy');
});
