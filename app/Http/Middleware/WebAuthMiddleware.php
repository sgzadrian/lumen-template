<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WebAuthMiddleware {

    public function handle( Request $request, Closure $next ) {
        if (
            isset( $request->app_key )
            &&
            strcmp( $request->app_key, env( 'WEB_KEY' ) ) === 0
        ) {
            return $next( $request );
        }
        return response()->json([ 'status' => 'Unauthorized App', ], 401 );
    }
}

