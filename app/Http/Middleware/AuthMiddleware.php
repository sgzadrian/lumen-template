<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use App\Models\User;

class AuthMiddleware {

  public function handle( $request, Closure $next, $guard = null ) {
    $token = $request->header( 'Token' );
    if ( !$token ) {
        $token = $request->header( 'Authorization' );
    }
    if ( !$token ) {
      return response()->json([ 'status' => 'Token not provided.' ], 401 );
    }
    try {
      $credentials = JWT::decode( $token, env('JWT_SECRET'), ['HS256'] );
    } catch( ExpiredException $e ) {
      return response()->json([ 'status' => 'Provided token is expired.' ], 401 );
    } catch( Exception $e ) {
      return response()->json([ 'status' => 'An error while decoding token.' ], 401 );
    }
    $user = User::findOrFail( $credentials->sub );
    $request->auth = $user;
    return $next( $request );
  }

}
