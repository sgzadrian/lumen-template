<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class AdminMiddleware {

    public function handle( Request $request, Closure $next ) {
        if ( $request->auth->type == User::NORMAL ) {
            return response()->json([
                'status' => "You don't have permission to access this resource",
            ], 401 );
        }
        return $next( $request );
    }
}

