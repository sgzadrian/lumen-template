<?php

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Models\User;

class AuthController extends BaseController {

    public function __construct(Request $request) {
        $this->request = $request;
    }

    protected function jwt( User $user ) {
        // Expiration time for 2 weeks or 3 hours if app is in production
        $time = strpos( env( 'APP_ENV' ), 'prod' ) === false ? 1209600 : 10800;
        $payload = [
            // Issuer of the token
            'iss' => env( 'APP_NAME' ),
            // Subject of the token
            'sub' => $user->id,
            // Time when JWT was issued.
            'iat' => time(),
            // Expiration time
            'exp' => time() + $time,
        ];
        return JWT::encode( $payload, env( 'JWT_SECRET' ) );
    }

    public function login(User $user) {
        $this->validate( $this->request, [
            'email' => 'required|email|min:3|max:190',
            'password' => 'required|min:6|max:80',
        ]);
        $user = User::where( 'email', $this->request->email )->first();
        if ( !$user ) {
            return response()->json([
                'status' => 'User does not exists.',
            ], 404);
        }
        if ( $user->status != User::ACTIVE ) {
            return response()->json([
                'error' => 'Access denied.',
            ], 403 );
        }
        if ( $user->checkPass( $this->request->password ) ) {
            return response()->json([
                'token' => $this->jwt( $user ),
                'type' => $user->type,
            ], 200 );
        }

        return response()->json([
            'status' => 'User or password is wrong.',
        ], 401 );
    }

}
