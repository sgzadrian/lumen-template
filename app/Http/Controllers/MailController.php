<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailExample;

class MailController extends Controller {

  private $request;

  public function __construct( Request $request ) {
    $this->request = $request;
  }

  public function send() {
    $this->validate( $this->request, [
      'email' => 'email|required',
      'name' => 'string|min:3|max:80|required',
      'phone' => 'regex:/[\d\s\+()]/|required',
      'message' => 'string|min:10|max:600|required',
    ]);
    Mail::to( 'example@mail.com' )->send( new EmailExample(
      $this->request->email,
      $this->request->name,
      $this->request->phone,
      $this->request->message
    ));
  }

}

