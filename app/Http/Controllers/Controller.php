<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController {

    /* Run this on the first deploy into production */
    public function flushCache() {
        Cache::flush();
        return response( 'OK', 200 );
    }

    public function clearCache( string $cacheKey = null ) {
        if ( $cacheKey ) {
            Cache::forget( $cacheKey );
        }
    }

    public function setCache(
        string $cacheKey,
        $value,
        int $ttl = 0
    ) {
        if ( $ttl > 0 ) {
            Cache::add( $cacheKey, $value, $ttl );
        } else {
            Cache::forever( $cacheKey, $value );
        }
    }

    public function getCache( string $cacheKey ) {
        $cache = false;
        /* Bypass cache load when not in production */
        if ( strpos( env( 'APP_ENV' ), 'prod' ) !== false ) {
            $cache = Cache::get( $cacheKey, false );
        }
        return $cache;
    }

    /* Request Helpers */
    protected function base_get(
        $model,
        string $cacheKey = null,
        bool $asData = false
    ) {
        $resp = null;
        if ( $cacheKey ) {
            $resp = $this->getCache( $cacheKey );
            if ( !$resp ) {
                $resp = $model::all();
                $this->setCache( $cacheKey, $resp );
            }
        } else {
            $resp = $model::all();
        }
        return $asData ? $resp : response()->json( $resp, 200 );
    }

    protected function base_show(
        $model,
        int $id,
        bool $asData = false
    ) {
        $resp = $model::findOrFail( $id );
        return $asData ? $resp : response()->json( $resp, 200 );
    }

    protected function base_store(
        Request $req,
        $model,
        array $rules,
        string $cacheKey = null
    ) {
        $this->validate( $req, $rules );
        $resp = response()->json([ 'status' => 'Error storing data' ], 500 );
        $obj = new $model;
        $obj->fill( $req->all() );
        if ( $obj->save() ) {
            $this->clearCache( $cacheKey );
            $resp = response()->json([
                'status' => 'OK',
                'id' => $obj->id,
            ], 201 );
        }
        return $resp;
    }

    protected function base_update(
        Request $req,
        $model,
        int $id,
        array $rules,
        string $cacheKey = null
    ) {
        $this->validate( $req, $rules );
        $obj = $model::findOrFail( $id );
        $resp = response()->json([ 'status' => 'Error updating data', ], 500);
        $obj->fill( $req->all() );
        if ( $obj->save() ) {
            $this->clearCache( $cacheKey );
            $resp = response()->json([ 'status' => 'OK', ], 200);
        }
        return $resp;
    }

    protected function base_delete(
        $model,
        int $id,
        string $cacheKey = null
    ) {
        $obj = $model::findOrFail( $id );
        $resp = response()->json([ 'status' => 'Error trying to delete the data', ], 500 );
        if ( $obj->delete() ) {
            $this->clearCache( $cacheKey );
            $resp = response()->json([ 'status' => 'OK', ], 200);
        }
        return $resp;
    }

    public function base_batch(
        Request $req,
        $model,
        array $rules,
        string $cacheKey = null
    ) {
        if ( sizeof( $req->batch ) == 0 ) {
            return response()->json([ 'status' => 'No items into request.', ], 204 );
        }
        $validation = Validator::make( $req->all(), [
            'batch' => 'required|array',
        ]);
        if ( $validation->fails() ) {
            return response()->json([
                'status' => 'Validation Error',
                'errors' => $validation->errors(),
            ], 422 );
        }
        $batch = $req->batch;
        $errors = 0;
        $errorMsg = [];
        foreach ( $batch as $item ) {
            $validation = Validator::make( $item, $rules );
            if ( $validation->fails() ) {
                ++$errors;
                array_push( $errorMsg, $validation->errors() );
            } else {
                $obj = $model::find( $item[ 'id' ] );
                $obj->fill( $item );
                $obj->save();
            }
        }
        if ( $errors == 0 ) {
            $this->clearCache( $cacheKey );
            return response()->json([ 'status' => 'OK' ], 200);
        }
        return response()->json([
            'status' => 'One or more errors occurred during update',
            'error_count' => $errors,
            'error_details' => $errorMsg,
        ], 400 );
    }

}
