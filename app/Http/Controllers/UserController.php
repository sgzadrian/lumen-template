<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    private $request;

    private $cacheKey = 'users';

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function index() {
        $users = $this->getCache( $this->cacheKey );
        if ( !$users ) {
            $users = User::where( 'type', '<>', User::ROOT )->get();
            $this->setCache( $this->cacheKey, $users );
        }
        return response()->json( $users, 200 );
    }

    public function show( $id = 0 ) {
        $user = User::findOrFail( $id );
        if ( $user->type == User::ROOT ) {
            return abort( 404 );
        }
        return response()->json( $user, 200 );
    }

    public function store() {
        $data = $this->request->all();
        $this->validate( $this->request, [
            'name' => 'required|string|min:3|max:190',
            'email' => 'required|email|unique:users,email|min:3|max:190',
            'password' => 'required|min:6|max:80',
            'type' => 'nullable|numeric|min:0|max:1',
        ]);
        $user = new User([
            'name' => $data[ 'name' ],
        ]);
        $user->email = $data[ 'email' ];
        $user->type = User::NORMAL;
        if (
            isset( $this->request->type )
            &&
            $this->request->type == User::ADMIN
        ) {
            $user->type = User::ADMIN;
        }
        $user->setPassword( $data[ 'password' ] );
        if ( $user->save() ) {
            $this->clearCache( $this->cacheKey );
            return response()->json([
                'status' => 'OK',
                'id' => $user->id,
            ], 201 );
        }
        return response()->json([ 'error' => 'Error creating user' ], 500 );
    }

    public function update( $id ) {
        $user = User::findOrFail( $id );
        if ( $user->type == User::ROOT ) {
            return abort( 404 );
        }
        $data = $this->request->all();
        $this->validate( $this->request, [
            'name' => 'string|min:3|max:190',
            'email' => 'email|min:3|max:190',
            'password' => 'nullable|min:6|max:80',
            'status' => 'boolean',
        ]);
        if ( strcmp( $user->email, $data[ 'email' ] ) != 0 ) {
            $validation = Validator::make( $data, [
                'email' => 'email|unique:users,email|min:3|max:190',
            ]);
            if ( $validation->fails() ) {
                return response()->json([
                    'status' => 'Validation Error',
                    'errors' => $validation->errors(),
                ], 422 );
            }
            $user->email = $data[ 'email' ];
            $user->deleteToken();
        }
        $user->fill( $data );
        if ( isset( $data[ 'password' ] ) ) {
            $user->setPassword( $data[ 'password' ] );
            $user->deleteToken();
        }
        $resp = response()->json([ 'status' => 'Error updating user', ], 500 );
        if ( $user->save() ) {
            $this->clearCache( $this->cacheKey );
            $resp = response()->json([ 'status' => 'OK', ], 200 );
        }
        return $resp;
    }

    public function destroy( $id ) {
        $user = User::findOrFail( $id );
        if ( $user->type != User::ROOT ) {
            if ( $user->delete() ) {
                $this->clearCache( $this->cacheKey );
                return response()->json([ 'status' => 'OK', ], 200 );
            }
        }
        return response()->json([ 'status' => 'Error trying to delete user', ], 500 );
    }

    public function batch() {
        $batch = $this->request->batch;
        $errors = 0;
        $errorMsg = [];
        foreach ( $batch as $data ) {
            $validation = Validator::make( $data, [
                'id' => 'required|exists:users,id',
                'name' => 'string|min:3|max:180',
                'email' => 'email|min:3|max:190',
                'password' => 'nullable|min:6|max:80',
                'status' => 'boolean',
            ]);
            if ( $validation->fails() ) {
                ++$errors;
                array_push( $errorMsg, $validation->errors() );
            } else {
                $user = User::find( $data[ 'id' ] );
                if ( $user->type != User::ROOT ) {
                    if ( strcmp( $user->email, $data[ 'email' ] ) != 0 ) {
                        $validation = Validator::make( $data, [
                            'email' => 'email|unique:users,email|min:3|max:190',
                        ]);
                        if ( $validation->fails() ) {
                            ++$errors;
                            array_push( $errorMsg, $validation->errors() );
                        }
                        $user->email = $data[ 'email' ];
                        $user->deleteToken();
                    }
                    $user->fill( $data );
                    $newPass = isset( $data[ 'password' ] ) ? $data[ 'password' ] : null;
                    if ( $newPass ) {
                        $user->setPassword( $newPass );
                        $user->deleteToken();
                    }
                    $status = $user->save();
                    if ( !$status ) {
                        ++$errors;
                        array_push( $errorMsg, [
                            'status' => 500,
                            'message' => 'Error updating user.',
                        ]);
                    }
                }
            }
        }
        $this->clearCache( $this->cacheKey );
        if ( $errors == 0 ) {
            return response()->json([ 'status' => 'OK' ], 200 );
        }
        return response()->json([
            'error_count' => $errors,
            'error_details' => $errorMsg,
        ], 400 );
    }

}
