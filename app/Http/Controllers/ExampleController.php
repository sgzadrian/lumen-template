<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller {

  private $request;

  public function __construct(Request $request) {
    $this->request = $request;
  }

  public function index() {
    // code...
  }

  public function show() {
    // code...
  }

  public function store() {
    // code...
  }

  public function update() {
    // code...
  }

  public function destroy() {
    // code...
  }

}
