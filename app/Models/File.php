<?php

namespace App\Models;

class File {

    private $server = null;
    private $folder = null;
    public $name = '';

    function __construct( $folder = null ) {
        $this->server = self::serverUrl();
        if ( $folder != null ) {
            $this->setFolder( $folder );
        }
    }

    public function setFolder( $folder ) {
        $this->folder = $folder;
        if ( !file_exists( $folder ) ) {
            mkdir( $folder, 0755, true );
        }
    }

    public function setName( $ext = null ) {
        $name = uniqid();
        if ( $ext != null ) {
            $name = $name .'.'. $ext;
        }
        $this->name = $name;
    }

    public function setPath( $path ) {
        $parts = explode( '/', $path );
        if ( sizeof( $parts ) == 2 ) {
            $this->folder = $parts[ 0 ];
            $this->name = $parts[ 1 ];
            return true;
        }
        return false;
    }

    public function getPath() {
        return "$this->folder/$this->name";
    }

    public function asset() {
        return $this->server . $this->getPath();
    }

    public function destroy() {
        $path = $this->getPath();
        if ( file_exists( $path )  ) {
            unlink( $path );
            $this->name = null;
            return true;
        }
        return false;
    }

    public static function getAsset( $filename ) {
        return self::serverUrl() . $filename;
    }

    public static function serverUrl() {
        $http = isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
        $server = "$http://$_SERVER[HTTP_HOST]/";
        return $server;
    }

}
