<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class User extends Model implements AuthenticatableContract, AuthorizableContract {
    use Authenticatable;
    use Authorizable;
    use HasFactory;

    const NORMAL = 0;
    const ADMIN  = 1;
    const ROOT   = 2;

    const ACTIVE   = 1;
    const UNACTIVE = 0;

    /* Mass assignable attributres. */
    protected $fillable = [
        'name',
        'status',
    ];

    /* NOT Mass assignable attributres. */
    protected $guarded = [
        'email',
        'type',
    ];

    /* Attributes hidden from arrays. */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* Attributes that should be cast to native types. */
    protected $casts = [
        'type' => 'integer',
        'status' => 'boolean',

    ];

    /* Relation functions */

    /* Helper functions */
    public function setPassword( $password ) {
      $this->password = Hash::make( $password );
    }

    public function checkPass( $password ) {
        return Hash::check( $password, $this->password ) ? true : false;
    }

    public function resetPassword() {
        $pass = Str::random(80);
        $this->setPassword( $pass );
        return $pass;
    }

}
