<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $users = User::count();
        if ( !$users ) {
            $isLocal = strpos( env( 'APP_ENV' ), 'prod' ) === false;
            $rootMail = !$isLocal ? 'soporte@hellcoders.com.mx' : 'admin@localhost';
            $admin = User::where( 'email', $rootMail )->first();
            if ( !$admin ) {
                $admin = new User([
                    'name' => 'Hellcoders',
                ]);
                $admin->email = $rootMail;
                $admin->type = User::ROOT;
                $admin->setPassword( env( 'CONTENT_KEY' ) );
                $admin->save();
            }
            if ( $isLocal ) {
                User::factory( 10 )->create();
            }
        }
    }
}
