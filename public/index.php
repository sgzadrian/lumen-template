<?php

header('Access-Control-Allow-Methods: GET, POST, PATCH, DELETE, OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: X-Requested-With, Access-Control-Request-Method, Content-Type, token, Authorization, Origin');
header('Access-Control-Allow-Origin: *');

// $headers = apache_request_headers();
// if ( isset( $headers['Origin'] ) ) { header( 'Access-Control-Allow-Origin: ' . $header['Origin'] ); }
// else if ( isset( $headers['Host'] ) ) { header( 'Access-Control-Allow-Origin: https://' . $header['Host'] ); }

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| First we need to get an application instance. This creates an instance
| of the application / container and bootstraps the application so it
| is ready to receive HTTP / Console requests from the environment.
|
*/

$app = require __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$app->run();
